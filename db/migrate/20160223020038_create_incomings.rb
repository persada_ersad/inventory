class CreateIncomings < ActiveRecord::Migration
  def change
    create_table :incomings do |t|
      t.integer :quantity
      t.date :date

      t.timestamps
    end
  end
end
