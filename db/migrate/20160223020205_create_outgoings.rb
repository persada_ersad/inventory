class CreateOutgoings < ActiveRecord::Migration
  def change
    create_table :outgoings do |t|
      t.integer :quantity
      t.date :date

      t.timestamps
    end
  end
end
