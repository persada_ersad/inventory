class AddProductIdToIncoming < ActiveRecord::Migration
  def change
    add_column :incomings, :product_id, :integer
  end
end
