class AddProductIdToOutgoing < ActiveRecord::Migration
  def change
    add_column :outgoings, :product_id, :integer
  end
end
