require 'test_helper'

class IncomingsControllerTest < ActionController::TestCase
  setup do
    @incoming = incomings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:incomings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create incoming" do
    assert_difference('Incoming.count') do
      post :create, incoming: { date: @incoming.date, quantity: @incoming.quantity }
    end

    assert_redirected_to incoming_path(assigns(:incoming))
  end

  test "should show incoming" do
    get :show, id: @incoming
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @incoming
    assert_response :success
  end

  test "should update incoming" do
    patch :update, id: @incoming, incoming: { date: @incoming.date, quantity: @incoming.quantity }
    assert_redirected_to incoming_path(assigns(:incoming))
  end

  test "should destroy incoming" do
    assert_difference('Incoming.count', -1) do
      delete :destroy, id: @incoming
    end

    assert_redirected_to incomings_path
  end
end
