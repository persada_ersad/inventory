class Product < ActiveRecord::Base
  has_many :outgoings
  has_many :incomings
  belongs_to :incoming

  def total_incoming_quantity
   self.incomings.sum(:quantity)
  end

  def total_outgoing_quantity
    self.outgoings.sum(:quantity)
  end
  def total_stock
    total_incoming_quantity - total_outgoing_quantity
  end

end
