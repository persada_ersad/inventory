class Outgoing < ActiveRecord::Base
  belongs_to  :product
  validate :check_stock


  def check_stock
    @product = Product.find_by(product_id)
    @product.total_stock
    if @product.total_stock <= quantity
      errors.add(:quantity,'out of limit stock ' )
    end
  end
  
end
