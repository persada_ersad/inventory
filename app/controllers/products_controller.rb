class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @products = Product.all
    respond_to do |format|
      format.html
      format.pdf do
        pdf = ProductPdf.new(@products)
        send_data pdf.render, filename: "product_#{@products}.pdf",
                              type: "application/pdf",
                              disposition: "inline"
      end
    end
  end

  def show

  end

  def new
    @product = Product.new
    respond_with(@product)
  end

  def edit
  end

  def create
    @product = Product.new(product_params)
    @product.save
    respond_with(@product)
  end

  def update
    @product.update(product_params)
    respond_with(@product)
  end

  def destroy
    @product.destroy
    respond_with(@product)
  end

  private
    def set_product
      @product = Product.find(params[:id])
    end

    def product_params
      params.require(:product).permit(:name, :price, :incoming_quantity, :quantity)
    end
end
