class OutgoingsController < ApplicationController
  before_action :set_outgoing, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @outgoings = Outgoing.all
    respond_with(@outgoings)
  end

  def show
    respond_with(@outgoing)
  end

  def new
    @outgoing = Outgoing.new
    respond_with(@outgoing)
  end

  def edit

  end

  def create
    @outgoing = Outgoing.new(outgoing_params)
    @outgoing.save
    respond_with(@outgoing)
  end

  def update
    @outgoing.update(outgoing_params)
    respond_with(@outgoing)
  end

  def destroy
    @outgoing.destroy
    respond_with(@outgoing)
  end

  private
    def set_outgoing
      @outgoing = Outgoing.find(params[:id])
    end

    def outgoing_params
      params.require(:outgoing).permit(:quantity, :date, :product_id)
    end
end
