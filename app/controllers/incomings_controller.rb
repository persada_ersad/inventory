class IncomingsController < ApplicationController
  before_action :set_incoming, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @incomings = Incoming.all
    respond_with(@incomings)
  end

  def show
    respond_with(@incoming)
  end

  def new
    @incoming = Incoming.new
    respond_with(@incoming)
  end

  def edit
  end

  def create
    @incoming = Incoming.new(incoming_params)
    @incoming.save
    respond_with(@incoming)
  end

  def update
    @incoming.update(incoming_params)
    respond_with(@incoming)
  end

  def destroy
    @incoming.destroy
    respond_with(@incoming)
  end

  private
    def set_incoming
      @incoming = Incoming.find(params[:id])
    end

    def incoming_params
      params.require(:incoming).permit(:quantity, :date, :product_id, :product_name, :name)
    end
end
