json.array!(@outgoings) do |outgoing|
  json.extract! outgoing, :id, :quantity, :date
  json.url outgoing_url(outgoing, format: :json)
end
