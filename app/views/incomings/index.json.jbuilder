json.array!(@incomings) do |incoming|
  json.extract! incoming, :id, :quantity, :date
  json.url incoming_url(incoming, format: :json)
end
