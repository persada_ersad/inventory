class ProductPdf < Prawn::Document

  def initialize(products)
    super(top_margin: 70)
    @products = products
    all_products
    line_products

  end
  def all_products
    text "All Products\##{@products.name}", size: 30, style: :bold
  end

  def line_products
    move_down 20
    table ([["ID","Product Name","Stock", "Price/pcs"]]) +
    @products.each.map do |products|
    ([products.id, products.name, products.total_stock, products.price])
    end
  end
end
