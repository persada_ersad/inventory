class ProductsPdf < Prawn::Document
  def initialize(products)
    super(top_margin: 70)
    @products = products
    all_products
    line_products

  end
  def all_products
    text "All Products\##{@products.name}", size: 30, style: :bold
  end

  def line_products
    move_down 20
    table_line_products
  end

  def table_line_products
    [["ID","Product Name","Stock", "Price/pcs"]]
    @products.line_products.map do |product|
      [product.id, product.name, product.total_stock, product.price]
    end
  end
end
